package id.co.diwaninvest.diwanapi.controller;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.Consultantdto;
import id.co.diwaninvest.diwanapi.entity.Consultant;
import id.co.diwaninvest.diwanapi.services.ConsultantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ConsultantController {

    @Autowired
    private ConsultantService consultantService;

    @GetMapping("/consultant")
    public List<Consultant> findAll(){
        return consultantService.findAll();
    }


    @GetMapping("/consultant/byid/{byconsultantid}")
    public ResponseEntity<ApiResponse> findByConsultantId(@PathVariable("byconsultantid") Long  consultanId){
        ApiResponse apiResponse = consultantService.findById(consultanId);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @PostMapping("/consultant")
    public ResponseEntity<ApiResponse> createConsultant(@RequestBody Consultantdto consultantdto){
        ApiResponse apiResponse = consultantService.createConsultant(consultantdto);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @PostMapping("/signinconsultant")
    public ResponseEntity<ApiResponse> signIn(@RequestBody Consultantdto consultantdto){
        ApiResponse apiResponse = consultantService.signInConsultant(consultantdto);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

}
