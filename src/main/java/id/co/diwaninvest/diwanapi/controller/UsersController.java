package id.co.diwaninvest.diwanapi.controller;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.SignInResponse;
import id.co.diwaninvest.diwanapi.dto.SignUpRequest;
import id.co.diwaninvest.diwanapi.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "*")
public class UsersController {

    @Autowired
    private UsersService usersServices;

    @GetMapping("/users")
    public ResponseEntity<ApiResponse> findAll(){
        ApiResponse apiResponse = usersServices.findAll();

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }


    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> signUp(@RequestBody @Valid SignUpRequest signUpRequest){
        ApiResponse apiResponse = usersServices.signUp(signUpRequest);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @PostMapping("/signin")
    public ResponseEntity<ApiResponse> signIn(@RequestBody @Valid SignInResponse signInResponse){
        ApiResponse apiResponse = usersServices.signIn(signInResponse);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @PutMapping("/setscoreandconsult")
    public ResponseEntity<ApiResponse> setScoreAndConsult(@RequestBody SignUpRequest usersdto){
        ApiResponse apiResponse = usersServices.setScoreandDateConsult(usersdto);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @PutMapping("/users")
    public ResponseEntity<ApiResponse> updateUser(@RequestBody SignUpRequest usersdto){
        ApiResponse apiResponse = usersServices.updateUser(usersdto);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }
}
