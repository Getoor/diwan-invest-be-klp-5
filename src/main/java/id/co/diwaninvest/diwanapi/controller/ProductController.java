package id.co.diwaninvest.diwanapi.controller;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.Productdto;
import id.co.diwaninvest.diwanapi.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/products/all")
    public ResponseEntity<ApiResponse> findAll(){
       ApiResponse apiResponse = productService.findAll();

       return ResponseEntity
               .status(apiResponse.getStatus())
               .body(apiResponse);
    }


    @PostMapping("/products")
    public ResponseEntity<ApiResponse> createProduct(@RequestBody Productdto productdto){
        ApiResponse apiResponse = productService.createProduct(productdto);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }


    @GetMapping("/products/byid/{byid}")
    public ResponseEntity<ApiResponse> findByProductId(@PathVariable("byid") Long productId){
        ApiResponse apiResponse = productService.findByIdProduct(productId);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @GetMapping("/products/bytype/{byproducttype}")
    public ResponseEntity<ApiResponse> findByProductType(@PathVariable("byproducttype") String productType){
        ApiResponse apiResponse = productService.findByProductType(productType);

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

    @GetMapping("/products/konsandmoderat")
    public ResponseEntity<ApiResponse> getDataModeratAndKonservatif(){
        ApiResponse apiResponse = productService.getDataModeratandKonsesrfatif();

        return ResponseEntity
                .status(apiResponse.getStatus())
                .body(apiResponse);
    }

}
