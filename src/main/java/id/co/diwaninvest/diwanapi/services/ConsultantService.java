package id.co.diwaninvest.diwanapi.services;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.Consultantdto;
import id.co.diwaninvest.diwanapi.entity.Consultant;
import id.co.diwaninvest.diwanapi.entity.Users;
import id.co.diwaninvest.diwanapi.repository.ConsultantRepository;
import id.co.diwaninvest.diwanapi.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ConsultantService {

    @Autowired
    private ConsultantRepository consultantRepository;

    @Autowired
    private UsersRepository usersRepository;

    public List<Consultant> findAll(){
        return consultantRepository.findAll();
    }

    public ApiResponse findById(Long  consultantid){
        ApiResponse apiResponse = new ApiResponse();
        Consultant consultant = consultantRepository.findByConsultantid(consultantid);

        if (consultant == null){
            apiResponse.setData("Consultant with Id " + consultantid + " Nof Found");
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }
        apiResponse.setData(consultant);
        return apiResponse;
    }


    public ApiResponse signInConsultant(Consultantdto consultantdto){
        ApiResponse apiResponse = new ApiResponse();

        Consultant consultant = consultantRepository.findOneByEmailIgnoreCaseAndPassword(consultantdto.getEmail(), consultantdto.getPassword());

        if (consultant == null){
            apiResponse.setData("Consultant Failed to login");
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }
        List<Users> users = usersRepository.findByConsultant_Consultantid(consultant.getConsultantid().longValue());
        consultant.setUsers(users);

        apiResponse.setData(consultant);
        return apiResponse;
    }


   public ApiResponse createConsultant(Consultantdto consultantdto){
        ApiResponse apiResponse = new ApiResponse();

        if (consultantdto.getName().equals("")){
            apiResponse.setData("Name is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getAddress().equals("")){
            apiResponse.setData("Address is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getRate().equals("")){
            apiResponse.setData("Rate is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getPhone().equals("")){
            apiResponse.setData("Phone is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getEmail().equals("")){
            apiResponse.setData("Email is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getPassword().equals("")){
            apiResponse.setData("Password is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getDescription().equals("")){
            apiResponse.setData("Description is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (consultantdto.getExperience().equals("")){
            apiResponse.setData("Consultant Experience is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }

        Consultant consultant = new Consultant();

        consultant.setName(consultantdto.getName());
        consultant.setAddress(consultantdto.getAddress());
        consultant.setRate(consultantdto.getRate());
        consultant.setPhonenumber(consultantdto.getPhone());
        Optional<Consultant> checkEmail = consultantRepository.findByEmail(consultantdto.getEmail());
        if (checkEmail.isPresent()){
           apiResponse.setData("Email has been register");
           apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
           return apiResponse;
        }
        consultant.setEmail(consultantdto.getEmail());
        consultant.setPassword(consultantdto.getPassword());
        consultant.setDescription(consultantdto.getDescription());
        consultant.setExperience(consultantdto.getExperience());

        consultant = consultantRepository.save(consultant);
        apiResponse.setData(consultant);

        return apiResponse;

   }
}
