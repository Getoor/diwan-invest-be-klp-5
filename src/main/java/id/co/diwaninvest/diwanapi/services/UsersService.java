package id.co.diwaninvest.diwanapi.services;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.SignInResponse;
import id.co.diwaninvest.diwanapi.dto.SignUpRequest;
import id.co.diwaninvest.diwanapi.entity.Consultant;
import id.co.diwaninvest.diwanapi.entity.Users;
import id.co.diwaninvest.diwanapi.repository.ConsultantRepository;
import id.co.diwaninvest.diwanapi.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ConsultantRepository consultantRepository;


    public ApiResponse findAll(){
        ApiResponse apiResponse = new ApiResponse();
        List<Users>users = usersRepository.findAll();

        apiResponse.setData(users);
        return apiResponse;
    }

    public ApiResponse updateUser(SignUpRequest signUpRequest){
        ApiResponse apiResponse = new ApiResponse();
        Users users = usersRepository.findByUserid(signUpRequest.getUserid());

        if (users == null){
            apiResponse.setData("Data user tidak di temukan");
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }

        if(signUpRequest.getName().equals("")){
            apiResponse.setData("Name is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getEmail().equals("")){
            apiResponse.setData("Email is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getPhone().equals("")){
            apiResponse.setData("Phone is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getAddress().equals("")){
            apiResponse.setData("Address is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        users.setName(signUpRequest.getName());
        users.setEmail(signUpRequest.getEmail());
        users.setPhonenumber(signUpRequest.getPhone());
        users.setAddress(signUpRequest.getAddress());

        users = usersRepository.save(users);
        apiResponse.setData(users);
        return apiResponse;

    }


    public ApiResponse setScoreandDateConsult(SignUpRequest signUpRequest){
        ApiResponse apiResponse = new ApiResponse();
        Users users = usersRepository.findByUserid(signUpRequest.getUserid());

        if (users == null){
            apiResponse.setData("Data user tidak di temukan");
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }
        Consultant consultant = consultantRepository.findByConsultantid(signUpRequest.getConsultanid());
        if (consultant == null){
            apiResponse.setData("Id Consultant Not Found");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (signUpRequest.getDateconsult().equals("")){
            apiResponse.setData("Masukan jadwal konsultasi");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (signUpRequest.getProductrecom().equals("")){
            apiResponse.setData("Masukan Recomendasi Product");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }

        users.setScore_quistioner(signUpRequest.getScorequis());
        users.setDate_consult(signUpRequest.getDateconsult());
        users.setProductrecom(signUpRequest.getProductrecom());
        users.setConsultant(consultant);
        users = usersRepository.save(users);
        apiResponse.setData(users);
        return apiResponse;

    }

    public ApiResponse signUp(SignUpRequest signUpRequest){

        ApiResponse apiResponse = new ApiResponse();

        if(signUpRequest.getName().equals("")){
            apiResponse.setData("Name is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getEmail().equals("")){
            apiResponse.setData("Email is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getPassword().equals("")){
            apiResponse.setData("Password is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if(signUpRequest.getPhone().equals("")){
            apiResponse.setData("Phone is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }

        if(signUpRequest.getAddress().equals("")){
            apiResponse.setData("Address is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }

        // bawa data dto ke entity user
        Users user = new Users();

        Date date = new Date();
        user.setName(signUpRequest.getName());
        user.setEmail(signUpRequest.getEmail());
        Optional<Users> checkEmail = usersRepository.findByEmail(signUpRequest.getEmail());
        if (checkEmail.isPresent()){
            apiResponse.setData("Email  Been Register");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        user.setPassword(signUpRequest.getPassword());
        user.setPhonenumber(signUpRequest.getPhone());
        user.setAddress(signUpRequest.getAddress());

        user.setCreate_at(date);
        user.setIs_active(false);
        user = usersRepository.save(user);
        apiResponse.setData(user);

        return apiResponse;

    }


    public ApiResponse signIn(SignInResponse signInResponse){
        ApiResponse apiResponse = new ApiResponse();

        Users users = usersRepository.findOneByEmailIgnoreCaseAndPassword(signInResponse.getEmail(), signInResponse.getPassword());

        // check apabila user dan password salah
        if (users == null){
            apiResponse.setData("User Failed to login");
            apiResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            return apiResponse;
        }

        apiResponse.setData(users);

        return apiResponse;
    }



}
