package id.co.diwaninvest.diwanapi.services;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.Productdto;
import id.co.diwaninvest.diwanapi.entity.Product;
import id.co.diwaninvest.diwanapi.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    public ApiResponse findAll(){
        ApiResponse apiResponse = new ApiResponse();
        List<Product> product = productRepository.findAll();
        apiResponse.setData(product);

        return apiResponse;
    }


    public ApiResponse getDataModeratandKonsesrfatif(){
        ApiResponse apiResponse = new ApiResponse();

        List<Product> products = productRepository.getDataModeratandKonsesrfatif();
        apiResponse.setData(products);
        return apiResponse;
    }


    public ApiResponse findByIdProduct(Long productId){
        ApiResponse apiResponse = new ApiResponse();

        Product product = productRepository.findByProductid(productId);
        if (product == null){
            apiResponse.setData("Tidak ada product dengan id " + productId);
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }

        apiResponse.setData(product);
        return apiResponse;
    }

    public ApiResponse findByProductType(String productType){
        ApiResponse apiResponse = new ApiResponse();

        List<Product> products = productRepository.findByProductType(productType);
        if (products.isEmpty()){
            apiResponse.setData("Tidak ada product dengan kategori " + productType);
            apiResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return apiResponse;
        }
        apiResponse.setData(products);
        return apiResponse;
    }




    public ApiResponse createProduct(Productdto productdto){
        ApiResponse apiResponse = new ApiResponse();

        if (productdto.getProduct().equals("")){
            apiResponse.setData("Product is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (productdto.getProductname().equals("")){
            apiResponse.setData("Product name is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (productdto.getDescription().equals("")){
            apiResponse.setData("Description is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (productdto.getProductdesc().equals("")){
            apiResponse.setData("Product Category is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (productdto.getProducturl().equals("")){
            apiResponse.setData("Product Url is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }
        if (productdto.getProducttype().equals("")){
            apiResponse.setData("Product Type is Required");
            apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return apiResponse;
        }


        Product product = new Product();
        product.setProduct(productdto.getProduct());
        product.setProductname(productdto.getProductname());
        product.setDescription(productdto.getDescription());
        product.setProductdesc(productdto.getProductdesc());
        product.setProducturl(productdto.getProducturl());
        product.setProductType(productdto.getProducttype());

        product = productRepository.save(product);

        apiResponse.setData(product);
        return apiResponse;
    }

}
