package id.co.diwaninvest.diwanapi.repository;

import id.co.diwaninvest.diwanapi.entity.Consultant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConsultantRepository extends JpaRepository<Consultant, String> {

    Consultant findByConsultantid(Long consultantid);

    Optional<Consultant> findByEmail(String email);

    Consultant findOneByEmailIgnoreCaseAndPassword(String email, String password);

}
