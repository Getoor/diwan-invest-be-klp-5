package id.co.diwaninvest.diwanapi.repository;

import id.co.diwaninvest.diwanapi.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, String> {

    List<Users> findByConsultant_Consultantid(Long consultantid);

    Users findOneByEmailIgnoreCaseAndPassword(String email, String password);
    Optional<Users> findByEmail(String email);

    Users findByUserid(Long userid);

}
