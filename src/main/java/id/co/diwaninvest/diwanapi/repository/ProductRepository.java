package id.co.diwaninvest.diwanapi.repository;

import id.co.diwaninvest.diwanapi.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findByProductid(Long productId);

    List<Product> findByProductType(String productType);

    @Query(value = "SELECT * FROM product WHERE product_type IN (\"moderat\",\"konservatif\")", nativeQuery = true)
    List<Product> getDataModeratandKonsesrfatif();

}
