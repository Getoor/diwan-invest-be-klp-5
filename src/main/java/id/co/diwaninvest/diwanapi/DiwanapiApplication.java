package id.co.diwaninvest.diwanapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiwanapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiwanapiApplication.class, args);
    }

}
