package id.co.diwaninvest.diwanapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "consultant")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Consultant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long consultantid;

    @Column(name = "name", length = 50)
    @NotNull(message = "Name is Required")
    @Size(min = 6, max = 50, message = "Name to short or to long")
    private String name;

    @Column(name = "address", length = 150)
    private String address;

    @Column(name = "rate", length = 6)
    @NotNull(message = "Rate is Required")
    private Integer rate;

    @Column(name = "phonenumber", length = 15)
    @NotNull(message = "Phone number is Required")
    private String phonenumber;

    @Column(name = "email", length = 50)
    @NotNull(message = "Email is Required")
    private String email;

    @NotNull(message = "Password is Required")
    @Column(name = "password")
    @JsonIgnore
    private String password;


    @Column(name = "description")
    @NotNull
    private String description;


    @Column(name = "consultant_experience", length = 20)
    @NotNull(message = "Experience is Required")
    private String experience;

    @OneToMany(mappedBy = "consultant")
    private List<Users> users;


}
