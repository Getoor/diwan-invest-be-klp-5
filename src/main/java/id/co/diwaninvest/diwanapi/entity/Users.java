package id.co.diwaninvest.diwanapi.entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userid;


    @NotNull(message = "Name is Required")
    @Column(name = "name", length = 100)
    private String name;

    @NotNull(message = "Email is Required")
    @Column(name = "email", length = 50, unique = true)
    @Email
    private String email;

    @NotNull(message = "Password is Required")
    @Column(name = "password")
    @JsonIgnore
    private String password;

    @NotNull(message = "Phone Number is Required")
    @Column(name = "phonenumber", length = 15)
    @Size(min = 10, max = 13, message = "Phone Number validation Character")
    private String phonenumber;

    @NotNull(message = "Address is Required")
    @Column(name = "address", length = 150)
    private String address;

    @Column(name = "scorequis")
    private Integer score_quistioner;

    @Column(name = "dateconsult")
    private String date_consult;

    @Column(name = "isactive")
    private Boolean is_active;

    @Column(name = "productrecom", length = 50)
    private String productrecom;

    @Column(name = "createat")
    @CreatedDate
    private Date create_at;

    @ManyToOne
    @JsonBackReference
    private Consultant consultant;


}
