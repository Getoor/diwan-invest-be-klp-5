package id.co.diwaninvest.diwanapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productid;

    @Column(name = "product")
    private String product;

    @Column(name = "product_name")
    private String productname;

    @Column(name = "description")
    private String description;

    @Column(name = "product_desc")
    private String productdesc;

    @Column(name = "product_url")
    private String producturl;

    @Column(name = "product_type")
    private String productType;


}
