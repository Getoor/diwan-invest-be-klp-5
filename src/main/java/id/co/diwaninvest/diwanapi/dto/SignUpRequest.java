package id.co.diwaninvest.diwanapi.dto;

import id.co.diwaninvest.diwanapi.entity.Consultant;
import lombok.Data;
@Data
public class SignUpRequest {

    private Long userid;
    private String name;
    private String email;
    private String password;
    private String phone;
    private String address;
    private Integer scorequis;
    private String  dateconsult;
    private String productrecom;
    private Consultant consultant;
    private Long consultanid;
}
