package id.co.diwaninvest.diwanapi.dto;

import lombok.Data;

@Data
public class Productdto {

    private Long productid;
    private String product;
    private String productname;
    private String description;
    private String productdesc;
    private String producturl;
    private String producttype;

}
