package id.co.diwaninvest.diwanapi.dto;

import id.co.diwaninvest.diwanapi.entity.Product;
import id.co.diwaninvest.diwanapi.entity.Users;
import lombok.Data;

import java.util.List;

@Data
public class Consultantdto {

    private Long consultantid;
    private String name;
    private String address;
    private Integer rate;
    private String phone;
    private String email;
    private String password;
    private String description;
    private String experience;
    private List<Users> users;
    private List<Product> products;

}
