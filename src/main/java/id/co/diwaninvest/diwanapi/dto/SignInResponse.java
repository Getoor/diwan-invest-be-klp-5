package id.co.diwaninvest.diwanapi.dto;

import lombok.Data;

@Data
public class SignInResponse {
    private String email;
    private String password;
}
