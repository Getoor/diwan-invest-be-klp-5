package id.co.diwaninvest.diwanapi.users;

import id.co.diwaninvest.diwanapi.entity.Users;
import id.co.diwaninvest.diwanapi.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UsersRepositoryTest {

    @Autowired
    private UsersRepository usersRepository;

    // user id berhasil di test
    @Test
    void findByUserid() {
        Users users = usersRepository.findByUserid(3L);
        Assertions.assertEquals("Geetoor Maven", users.getName());
    }

    // user id tidak di temuakan
    @Test
    void notFoundfindByuserId() {
        Users users = usersRepository.findByUserid(3L);
        Assertions.assertNotEquals("Not found", users.getName());
    }

    // userEmail berhasil di test
    @Test
    void findByUserEmail() {
        Optional<Users> users = usersRepository.findByEmail("mvn@gmail.com");
        Assertions.assertTrue(users.isPresent());
    }

    // userEmail ketika tidak di temukan akan false
    @Test
    void notFoundUserEmail() {
        Optional<Users> users = usersRepository.findByEmail("notfound@gmail.com");
        Assertions.assertFalse(users.isPresent());
    }

    // find email and password berhasil di test dan tidak null
    @Test
    void findEmailAndPassword() {
        Users emailAndPassword = usersRepository.findOneByEmailIgnoreCaseAndPassword("mvn@gmail.com", "123");
        Assertions.assertNotNull(emailAndPassword);
    }

    // find email and password tidak di temukan dan null
    @Test
    void notFoundEmailAndPassword() {
        Users emailAndPassword = usersRepository.findOneByEmailIgnoreCaseAndPassword("notfound@gmail.com", "123");
        Assertions.assertNull(emailAndPassword);
    }
}