package id.co.diwaninvest.diwanapi.users;

import id.co.diwaninvest.diwanapi.dto.ApiResponse;
import id.co.diwaninvest.diwanapi.dto.SignInResponse;
import id.co.diwaninvest.diwanapi.dto.SignUpRequest;
import id.co.diwaninvest.diwanapi.services.UsersService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class UsersServiceTest {

    @Autowired
    private UsersService usersService;


    // login test success
    @Test
    void signInSuccess() {
        SignInResponse signInResponse = new SignInResponse();
        signInResponse.setEmail("mvn@gmail.com");
        signInResponse.setPassword("123");
        ApiResponse user = usersService.signIn(signInResponse);
        Assertions.assertEquals(200, user.getStatus());
    }

    // failed to login test
    @Test
    void signInNotSuccess() {
        SignInResponse signInResponse = new SignInResponse();
        signInResponse.setEmail("not@gmail.com");
        signInResponse.setPassword("123");
        ApiResponse user = usersService.signIn(signInResponse);
        Assertions.assertEquals(401, user.getStatus());
    }


    // user register berhasil di test
    @Test
    void signUpSuccess() {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("Unit Test");
        signUpRequest.setEmail("test@gmail.com");
        signUpRequest.setPassword("123");
        signUpRequest.setPhone("08976567829");
        signUpRequest.setAddress("Kabupaten Testing area");

        ApiResponse user = usersService.signUp(signUpRequest);
        Assertions.assertNotNull(user);
    }

    // test ketika user mempunyai email yang sama
    @Test
    void signUpIfUserHaveEmailSame() {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setName("Unit Test");
        signUpRequest.setEmail("mvn@gmail.com");
        signUpRequest.setPassword("123");
        signUpRequest.setPhone("08976567829");
        signUpRequest.setAddress("Kabupaten Testing area");

        ApiResponse user = usersService.signUp(signUpRequest);
        Assertions.assertEquals("Email  Been Register", user.getData());
    }
}