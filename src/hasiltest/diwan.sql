-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 25, 2022 at 07:05 PM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diwan`
--

-- --------------------------------------------------------

--
-- Table structure for table `consultant`
--

CREATE TABLE `consultant` (
  `consultantid` bigint(20) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `consultant_experience` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phonenumber` varchar(15) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `consultant`
--

INSERT INTO `consultant` (`consultantid`, `address`, `description`, `email`, `consultant_experience`, `name`, `password`, `phonenumber`, `rate`) VALUES
(2, 'Kabupaten Sinjai, Sulawesi-Selatan Indonesia', 'Agus Kurniawan adalah Salah satu konsultan Diwan Invest yang telah berpengalaman mengenai investasi, banyak saran jika anda ingin berkonsultasi dengannya.', 'agus@gmail.com', '8 Tahun', 'Agus Kurniawan', 'qwerty', '089516141980', 5),
(3, 'Kabupaten Sinjai, Sulawesi-Selatan Indonesia', 'Supriadi Wibowo merupakan orang yang pernah terlibat dalam penelitian saham yang ada di luar negeri dan sudah sangat terpercaya untuk mengikuti arahannya untuk berkonsultasi.', 'obo@gmail.com', '5 Tahun', 'Supriadi Wibowo', 'qwerty', '089516141980', 5),
(4, 'Kabupaten Sinjai, Sulawesi-Selatan Indonesia', 'Achyar Fatahillah Tinggal Di Makassar dan sudah menyelesaikan sekolahnya di luar negeri dengan jurusan Investasi. Jika ingin berkonsultasi, silahkan pilih achyar.', 'achyar@gmail.com', '7 Tahun', 'Achyar Fatahillah', 'qwerty', '089516141980', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `productid` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `product_desc` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`productid`, `description`, `product`, `product_type`, `product_desc`, `product_name`, `product_url`) VALUES
(1, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'Indodax adalah sebuah perusahaan berbasis teknologi yang mempertemukan penjual dan pembeli aset digital terbesar di Indonesia.', 'INDODAX', 'https://indodax.com'),
(2, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'Stockbit adalah aplikasi untuk kamu yang ingin berdiskusi, analisa dan investasi saham dalam satu tempat.', 'STOCKBIT', 'https://stockbit.com/'),
(3, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'Bibit adalah perusahaan finansial teknologi milik PT Bibit Tumbuh Bersama yang menyediakan portal jual beli reksadana untuk memperjual-belikan produk investasi reksadana secara daring dari berbagai perusahaan manajer investasi', 'BIBIT', 'https://bibit.id/'),
(4, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'Indo Premier Sekuritas adalah sekuritas swasta terbesar di Indonesia menawarkan investasi di pasar modal dengan melayani klien individu maupun korporasi berdasarkan izin Otoritas Jasa Keuangan', 'IPOT', 'https://indopremier.com/#ipot/home'),
(5, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'Ajaib adalah aplikasi investasi yang memudahkanmu untuk berinvestasi di reksa dana dan saham secara online. Ajaib memberikan pilihan reksa dana terbaik dan', 'AJAIB', 'https://ajaib.co.id/'),
(6, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'HOTS adalah aplikasi perdagangan saham yang paling mudah digunakan dan dirancang untuk dapat mengeksekusi order dengan cepat dan stabil ', 'Mirae HOTS', 'https://hots.miraeasset.co.id/'),
(7, 'Saham adalah bukti kepemilikan atas suatu perusahaan yang mana bukti tersebut dapat berbentuk warkat (dokumen) atau tanpa warkat (pencatatan secara elektronik).', 'SAHAM', 'agresif', 'POEMS merupakan sistem online trading pertama di Indonesia yang menyediakan transaksi saham, reksa dana, dan ETF hanya dengan satu akun dalam satu platform.', 'POEMS', 'https://www.poems.co.id/'),
(8, 'Surat berharga yang diterbitkan sebagai bukti tanda hutang kepada pemegang obligasi, berjangka Panjang ( > 1 tahun ), diterbitkan atas unjuk, suku bunga fixed atau float.', 'OBLIGASI', 'moderat', 'Obligasi / Surat Utang Surat berharga yang diterbitkan sebagai bukti tanda hutang kepada pemegang obligasi, berjangka Panjang ( > 1 tahun ).', 'Mandiri Obligasi', 'https://bankmandiri.co.id/investasi-obligasi'),
(9, 'Surat berharga yang diterbitkan sebagai bukti tanda hutang kepada pemegang obligasi, berjangka Panjang ( > 1 tahun ), diterbitkan atas unjuk, suku bunga fixed atau float.', 'OBLIGASI', 'moderat', 'Obligasi Negara Ritel Seri ORI022 adalah Surat Utang Negara yang dijual oleh Pemerintah kepada investor ritel di Pasar Perdana Domestik, diterbitkan tanpa warkat, dan dapat diperdagangkan di Pasar Sekunder.', 'Bni Obligasi', 'https://www.bni.co.id/id-id/wealth/produkinvestasi/obligasi'),
(10, 'Surat berharga yang diterbitkan sebagai bukti tanda hutang kepada pemegang obligasi, berjangka Panjang ( > 1 tahun ), diterbitkan atas unjuk, suku bunga fixed atau float.', 'OBLIGASI', 'moderat', 'Obligasi Negara yang dijual kepada individu atau perseorangan WNI melalui Agen Penjual dengan volume minimum yang telah ditentukan.', 'Bri Obligasi', 'https://bri.co.id/web/wealth-management/obligasi-ori'),
(11, 'Surat berharga yang diterbitkan sebagai bukti tanda hutang kepada pemegang obligasi, berjangka Panjang ( > 1 tahun ), diterbitkan atas unjuk, suku bunga fixed atau float.', 'OBLIGASI', 'moderat', 'Danamon Online Banking. Obligasi adalah instrumen hutang dalam bentuk sekuritas yang diterbitkan oleh pemerintah, korporasi, atau penerbit lainnya. Kupon dan pokok dijamin oleh Undang-Undang dan dibayarkan dengan tingkat bunga tetap.', 'Danamon Obligasi', 'https://www.danamon.co.id/id/Personal/Investasi/Obligasi'),
(12, 'Deposito merupakan produk penyimpanan uang yang disediakan oleh bank dengan sistem penyetoran yang dilakukan di awal serta memiliki ketentuan penarikan yang hanya bisa dilakukan sesuai dengan ketentuan penarikan.', 'DEPOSITO', 'konservatif', 'Mandiri Deposito Rupiah adalah simpanan berjangka dalam mata uang Rupiah dengan bunga menarik dan beragam keuntungan lainnya.', 'Mandiri deposito', 'https://www.bankmandiri.co.id/deposito-rupiah-mandiri'),
(13, 'Deposito merupakan produk penyimpanan uang yang disediakan oleh bank dengan sistem penyetoran yang dilakukan di awal serta memiliki ketentuan penarikan yang hanya bisa dilakukan sesuai dengan ketentuan penarikan.', 'DEPOSITO', 'konservatif', 'BNI Deposito merupakan simpanan berjangka yang menjadikan simpanan Anda aman dengan tingkat suku bunga yang menarik.', 'Bni deposito', 'https://www.bni.co.id/id-id/individu/simpanan-berjangka-dplk/bni-deposito'),
(14, 'Deposito merupakan produk penyimpanan uang yang disediakan oleh bank dengan sistem penyetoran yang dilakukan di awal serta memiliki ketentuan penarikan yang hanya bisa dilakukan sesuai dengan ketentuan penarikan.', 'DEPOSITO', 'konservatif', 'Deposito Internet Banking BRI merupakan produk deposito yang menawarkan suku bunga yang kompetitif.', 'BRI deposito', 'https://bri.co.id/simulasi-deposito'),
(15, 'Deposito merupakan produk penyimpanan uang yang disediakan oleh bank dengan sistem penyetoran yang dilakukan di awal serta memiliki ketentuan penarikan yang hanya bisa dilakukan sesuai dengan ketentuan penarikan.', 'DEPOSITO', 'konservatif', 'UOB Deposito adalah produk deposito berjangka dari Bank UOB dengan tenor mulai dari satu hingga 12 bulan. UOB Deposito tersedia dalam mata yang Rupiah dan valas (asing).', 'Uob Deposito', 'https://www.tmrwbyuob.com/id/id/products/tmrw-powersaver.html'),
(16, 'Deposito merupakan produk penyimpanan uang yang disediakan oleh bank dengan sistem penyetoran yang dilakukan di awal serta memiliki ketentuan penarikan yang hanya bisa dilakukan sesuai dengan ketentuan penarikan.', 'DEPOSITO', 'konservatif', 'Penjelasan Produk Deposito adalah simpanan berjangka yang menguntungkan dan aman dengan suku bunga tinggi yang memberikan keleluasaan pilihan jangka waktu dan mata uang yang diselenggarakan oleh PT. Bank Danamon Indonesia', 'Danamon Deposito', 'https://www.danamon.co.id/id/Personal/Simpanan/Deposito-Konvensional'),
(17, 'Asuransi adalah pertanggungan atau perjanjian antara dua belah pihak, dimana pihak satu berkewajiban membayar iuran/kontribusi/premi.', 'ASURANSI', 'konservatif', 'PT AXA Mandiri Financial Services adalah anak usaha Bank Mandiri yang berbisnis di bidang asuransi jiwa. Untuk mendukung kegiatan bisnisnya, hingga akhir tahun 2021', 'AXA MANDIRI', 'https://axa-mandiri.co.id/'),
(18, 'Asuransi adalah pertanggungan atau perjanjian antara dua belah pihak, dimana pihak satu berkewajiban membayar iuran/kontribusi/premi.', 'ASURANSI', 'konservatif', 'Asuransi Jiwa BCA atau yang lebih dikenal dengan BCA Life adalah perusahaan asuransi jiwa yang berdiri sejak tahun 2014 dan berkantor pusat di Jakarta. Perusahaan tergabung dalam grup BCA.', 'BCA LIFE', 'https://www.bcalife.co.id/');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` bigint(20) NOT NULL,
  `address` varchar(150) NOT NULL,
  `createat` datetime(6) DEFAULT NULL,
  `dateconsult` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `isactive` bit(1) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phonenumber` varchar(13) NOT NULL,
  `productrecom` varchar(50) DEFAULT NULL,
  `scorequis` int(11) DEFAULT NULL,
  `consultant_consultantid` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `address`, `createat`, `dateconsult`, `email`, `isactive`, `name`, `password`, `phonenumber`, `productrecom`, `scorequis`, `consultant_consultantid`) VALUES
(3, 'Kabupaten Sinjai Sulawesi Selatan', '2022-12-23 00:06:22.487000', '2022-12-24', 'mvn@gmail.com', b'0', 'Geetoor Maven', '123', '082225637368', 'moderat', 83, 4),
(4, 'Kabupaten Enrekang Sulawesi Selatan', '2022-12-23 00:38:11.014000', '2022-12-24', 'sls@gmail.com', b'0', 'SulistiyaWati Nardi', '123', '089516141980', 'moderat', 85, 2),
(5, 'Kabupaten Sinjai', '2022-12-23 00:41:33.319000', '2022-12-24', 'riswan@gmail.com', b'0', 'Riswan Syahaider', '123', '089516141980', 'konservatif', 76, 3),
(6, 'Kota Yogyakarta Jawa Tengah', '2022-12-23 19:31:35.147000', '2022-12-24', 'dwi@gmail.com', b'0', 'Akhmad Dwi Hermawan', '123', '089516141980', 'moderat', 88, 2),
(7, 'Kabupaten Sinjai Sulawesi selatan', '2022-12-24 00:31:15.287000', '2022-12-25', 'fathur@gmail.com', b'0', 'Fathur Ramadhan', '123', '082225637368', 'agresif', 99, 3),
(8, 'Kabupaten Sinjai Sulawesi Selatan', '2022-12-25 15:02:21.409000', '2022-12-26', 'ikbal@gmail.com', b'0', 'Ikbal Febriansyah', '123', '082225637368', 'agresif', 95, 4),
(13, 'Jakarta Selatan Ajah', '2022-12-25 19:16:34.306000', '2022-12-26', 'dendi@gmail.com', b'0', 'Dendy Ajah', '123', '082225637368', 'agresif', 95, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultant`
--
ALTER TABLE `consultant`
  ADD PRIMARY KEY (`consultantid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`),
  ADD KEY `FKpw2c750568s3d6uto61fsphbk` (`consultant_consultantid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultant`
--
ALTER TABLE `consultant`
  MODIFY `consultantid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `productid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FKpw2c750568s3d6uto61fsphbk` FOREIGN KEY (`consultant_consultantid`) REFERENCES `consultant` (`consultantid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
